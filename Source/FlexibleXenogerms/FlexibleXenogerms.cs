﻿using HarmonyLib;
using Verse;

namespace PorousBoat.FlexibleXenogerms
{
    [StaticConstructorOnStartup]
    public class FlexibleXenogerms
    {
        static FlexibleXenogerms()
        {
            Log.Message("FLEXIBLE XENOGERMS: Running...");
            harmonyInstance = new Harmony("PorousBoat.FlexibleXenogerms");
            harmonyInstance.PatchAll();
        }

        public static Harmony harmonyInstance;
    }
}