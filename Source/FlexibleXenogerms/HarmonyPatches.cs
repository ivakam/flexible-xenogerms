using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using RimWorld;
using Verse;

namespace PorousBoat.FlexibleXenogerms
{
    [HarmonyPatch(typeof(Ideo), "IsPreferredXenotype")]
    public class IdeoPatches
    {
        static void Postfix(Ideo __instance, ref bool __result, Pawn pawn)
        {
            bool res = __result;
            if (!__instance.PreferredXenotypes.Any() && !__instance.PreferredCustomXenotypes.Any())
            {
                return;
            }
            if (pawn.genes == null)
            {
                return;
            }

            if (pawn.genes.Xenotype != null && pawn.Ideo.HasMeme((MemeDefOf.Transhumanist)))
            {
                foreach (XenotypeDef t in __instance.PreferredXenotypes)
                {
                    List<GeneDef> endogenes = pawn.genes.Endogenes.Select(x => x.def).ToList();
                    if (endogenes.Equals(t.genes))
                    {
                        __result = true;
                        return;
                    }
                }
            }

            if (pawn.genes.CustomXenotype != null)
            {
                foreach (CustomXenotype t in __instance.PreferredCustomXenotypes)
                {
                    List<GeneDef> endogenes = pawn.genes.Endogenes.Select(x => x.def).ToList();
                    if (endogenes.Equals(t.genes))
                    {
                        __result = true;
                        return;
                    }
                }
            }
        }
    }

    [HarmonyPatch(typeof(Xenogerm), "PawnIdeoDisallowsImplanting")]
    public class XenogermPatches
    {
        static void Postfix(ref bool __result, Pawn selPawn)
        {
            if (!IdeoUtility.DoerWillingToDo(HistoryEventDefOf.BecomeNonPreferredXenotype, selPawn))
            {
                if (selPawn.Ideo.HasMeme(MemeDefOf.Transhumanist))
                {
                    __result = false;
                }
            }
        }
    }
}